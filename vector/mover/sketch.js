/*
 * sketch.js
 * */

function Mover(position, velocity) {

    this.position = position;
    this.velocity = velocity;

    this.update = function() {

        /*
         * 端に行ったら跳ね返る処理
         * */
        if(this.position.x < 0 || this.position.x > width) {
            this.velocity.x *= -1;
            this.position.x = constrain(this.position.x, 0, width);
        }
        if(this.position.y < 0 || this.position.y > height) {
            this.velocity.y *= -1;
            this.position.y = constrain(this.position.y, 0, height);
        }

        // 位置に変化量を足すことで図形の動きを実現する
        this.position = p5.Vector.add(this.position, this.velocity);
    };

    this.display = function() {
        push();

        fill(color(250, 250, 250));
        ellipse(this.position.x, this.position.y, 30, 30);

        pop();
    };

}

var mover;

function setup() {

    createCanvas(500, 500);

    mover = new Mover(new p5.Vector(width / 2, height / 2), new p5.Vector(random(-5, 5), random(-5, 5)));

}

function draw() {
    background(color(120, 120, 120));

    mover.update();
    mover.display();
}

function mousePressed() {
    /*
     * マウスクリックされるたびに
     * moverの動く量にランダムのスカラー量を掛ける
     * */
    mover.velocity = p5.Vector.mult(mover.velocity, random(0.5, 1.5));
}

