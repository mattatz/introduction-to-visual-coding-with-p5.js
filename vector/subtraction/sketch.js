/*
 * sketch.js
 * */

function visualizeVector(origin, vector, strokeColor) {

    var end = p5.Vector.add(origin, vector);

    push();

    noStroke();
    fill(color(0, 0, 0));
    ellipse(origin.x, origin.y, 5, 5);
    
    strokeColor = strokeColor || color(100, 100, 100);
    stroke(strokeColor);
    line(origin.x, origin.y, end.x, end.y);

    var dir = p5.Vector.sub(origin, end);
    var t = dir.heading();

    var len = min(dir.mag() * 0.5, 8);

    var t1 = t - PI / 8;
    var p0 = new p5.Vector(cos(t1) * len, sin(t1) * len);

    var t2 = t + PI / 8;
    var p1 = new p5.Vector(cos(t2) * len, sin(t2) * len);

    line(end.x, end.y, end.x + p0.x, end.y + p0.y);
    line(end.x, end.y, end.x + p1.x, end.y + p1.y);

    pop();
}

function setup() {

    createCanvas(500, 500);

    var v1 = new p5.Vector(10, 20);
    var v2 = new p5.Vector(30, 3);

    var v3 = p5.Vector.sub(v1, v2);
    console.log(v3);

    visualizeVector(new p5.Vector(width / 2, 50), v1);
    visualizeVector(new p5.Vector(width / 2, 100), v2);
    visualizeVector(new p5.Vector(width / 2, 150), v3);

    // 
    visualizeVector(new p5.Vector(width / 2, 200), v1, color(255, 0, 0));
    visualizeVector(new p5.Vector(width / 2, 200), v2, color(0, 0, 255));
    visualizeVector(new p5.Vector(width / 2, 200), v3, color(0, 255, 0));

}

function draw() {
}

