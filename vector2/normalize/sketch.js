/*
 * sketch.js
 * */

function visualizeVector(origin, vector, strokeColor) {

    var end = p5.Vector.add(origin, vector);

    push();

    noStroke();
    fill(color(0, 0, 0));
    ellipse(origin.x, origin.y, 5, 5);
    
    strokeColor = strokeColor || color(100, 100, 100);
    stroke(strokeColor);
    line(origin.x, origin.y, end.x, end.y);

    var dir = p5.Vector.sub(origin, end);
    var t = dir.heading();

    var len = min(dir.mag() * 0.5, 8);

    var t1 = t - PI / 8;
    var p0 = new p5.Vector(cos(t1) * len, sin(t1) * len);

    var t2 = t + PI / 8;
    var p1 = new p5.Vector(cos(t2) * len, sin(t2) * len);

    line(end.x, end.y, end.x + p0.x, end.y + p0.y);
    line(end.x, end.y, end.x + p1.x, end.y + p1.y);

    pop();
}

function Mover(radius, position, velocity) {

    this.radius = radius;
    this.position = position;
    this.velocity = velocity;

    this.color = color(250, 250, 250);

    this.update = function() {
        // 位置に変化量を足すことで図形の動きを実現する
        this.position = p5.Vector.add(this.position, this.velocity);
    };

    this.display = function() {
        push();

        fill(this.color);

        ellipseMode(CENTER);

        var diameter = this.radius * 2;
        ellipse(this.position.x, this.position.y, diameter, diameter);

        pop();
    };

}

var mover;

function setup() {

    createCanvas(500, 500);

    mover = new Mover(10, new p5.Vector(width / 2, height / 2), new p5.Vector(0, 0));

}

function draw() {
    background(255);

    // カーソルの位置ベクトル
    var mouse = new p5.Vector(mouseX, mouseY);

    // center位置ベクトルからmouse位置ベクトルへと向かう幾何ベクトル
    var dir = p5.Vector.sub(mouse, mover.position);

    dir.normalize(); // 大きさが1の単位ベクトルへの変換

    // moverの速度に単位ベクトルのdirを設定することで、
    // カーソルの方向に同じ速度(1)で動くようにする
    mover.velocity = dir;

    mover.update();
    mover.display();
}

