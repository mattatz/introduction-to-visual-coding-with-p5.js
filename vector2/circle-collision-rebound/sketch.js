/*
 * sketch.js
 * */

function visualizeVector(origin, vector, strokeColor) {

    var end = p5.Vector.add(origin, vector);

    push();

    noStroke();
    fill(color(0, 0, 0));
    ellipse(origin.x, origin.y, 5, 5);
    
    strokeColor = strokeColor || color(100, 100, 100);
    stroke(strokeColor);
    line(origin.x, origin.y, end.x, end.y);

    var dir = p5.Vector.sub(origin, end);
    var t = dir.heading();

    var len = min(dir.mag() * 0.5, 8);

    var t1 = t - PI / 8;
    var p0 = new p5.Vector(cos(t1) * len, sin(t1) * len);

    var t2 = t + PI / 8;
    var p1 = new p5.Vector(cos(t2) * len, sin(t2) * len);

    line(end.x, end.y, end.x + p0.x, end.y + p0.y);
    line(end.x, end.y, end.x + p1.x, end.y + p1.y);

    pop();
}

function Mover(radius, position, velocity) {

    this.radius = radius;
    this.position = position;
    this.velocity = velocity;

    this.color = color(250, 250, 250);

    this.update = function() {

        // 色をリセット
        this.color = color(250, 250, 250);

        /*
         * 端に行ったら跳ね返る処理
         * */
        if(this.position.x < 0 || this.position.x > width) {
            this.velocity.x *= -0.9;
            this.position.x = constrain(this.position.x, 0, width);
        }
        if(this.position.y < 0 || this.position.y > height) {
            this.velocity.y *= -0.9;
            this.position.y = constrain(this.position.y, 0, height);
        }

        // 位置に変化量を足すことで図形の動きを実現する
        this.position = p5.Vector.add(this.position, this.velocity);
    };

    this.collide = function(other) {

        // otherの位置ベクトルから、thisの位置ベクトルへと向かうベクトルd
        var d = p5.Vector.sub(other.position, this.position);

        // ベクトルdの大きさ = thisとotherとの距離
        var m = d.mag();

        // thisとotherとで最低限保たれているべき距離（これ以上近づいていると反発しあう必要がある）
        var minDist = this.radius + other.radius;

        if(m < minDist) {
            this.color = color(10, 10, 10);

            // otherがいるべき位置ベクトルを算出
            var target = p5.Vector.add(this.position, d.setMag(minDist)); 

            // otherの現在位置からotherがいるべき位置へと向かうベクトルを反発力として算出する
            var bounce = p5.Vector.sub(target, other.position).mult(0.05); 

            // 反発力を速度に加算
            this.velocity = p5.Vector.sub(this.velocity, bounce); 
        }

    };

    this.display = function() {
        push();

        fill(this.color);

        ellipseMode(CENTER);

        var diameter = this.radius * 2;
        ellipse(this.position.x, this.position.y, diameter, diameter);

        visualizeVector(this.position, p5.Vector.mult(this.velocity, 10));

        pop();
    };

}

var movers = [];

function setup() {

    createCanvas(700, 500);

    for(var i = 0; i < 20; i++) {
        var mover = new Mover(random(5, 20), new p5.Vector(random(0, width), random(0, height)), new p5.Vector(random(-2, 2), random(-2, 2)));
        movers.push(mover);
    }

}

function draw() {
    background(255);

    for(var i = 0; i < movers.length; i++) {
        var mover = movers[i];
        mover.update();

        // 衝突判定
        for(var j = 0; j < movers.length; j++) {
            if(i == j) continue;

            var other = movers[j];
            mover.collide(other);
        }

        mover.display();
    }

}

