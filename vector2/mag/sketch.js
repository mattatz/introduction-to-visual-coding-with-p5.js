/*
 * sketch.js
 * */

function visualizeVector(origin, vector, strokeColor) {

    var end = p5.Vector.add(origin, vector);

    push();

    noStroke();
    fill(color(0, 0, 0));
    ellipse(origin.x, origin.y, 5, 5);
    
    strokeColor = strokeColor || color(100, 100, 100);
    stroke(strokeColor);
    line(origin.x, origin.y, end.x, end.y);

    var dir = p5.Vector.sub(origin, end);
    var t = dir.heading();

    var len = min(dir.mag() * 0.5, 8);

    var t1 = t - PI / 8;
    var p0 = new p5.Vector(cos(t1) * len, sin(t1) * len);

    var t2 = t + PI / 8;
    var p1 = new p5.Vector(cos(t2) * len, sin(t2) * len);

    line(end.x, end.y, end.x + p0.x, end.y + p0.y);
    line(end.x, end.y, end.x + p1.x, end.y + p1.y);

    pop();
}

var center;

function setup() {

    createCanvas(700, 500);

    // 画面の真ん中の位置ベクトル
    center = new p5.Vector(width / 2, height / 2);

}

function draw() {
    background(255);

    // カーソルの位置ベクトル
    var mouse = new p5.Vector(mouseX, mouseY);

    // center位置ベクトルからmouse位置ベクトルへと向かう幾何ベクトル
    var dir = p5.Vector.sub(mouse, center);

    // dirベクトルの大きさ
    var m = dir.mag();

    text(m, width / 2 + 10, height / 2 - 10);

    visualizeVector(center, dir);

}

