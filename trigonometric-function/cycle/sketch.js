/*
 * sketch.js
 * */

function setup() {

    createCanvas(500, 500);

}

function draw() {

    background(255);

    /*
     * sin(frameCount * 0.1)は-1から1の間を反復する
     * sin()の戻り値に100をかけることで-100から100の間を反復するようにする
     * */
    var dx = sin(frameCount * 0.1) * 100;

    var x = width / 2 + dx;
    var y = height / 2;

    fill(color(255, 0, 0));
    ellipse(x, y, 50, 50);
  
}

