/*
 * sketch.js
 * */

var controller = {
    layers      : 12,
    samples     : 20
};

function setup() {

    createCanvas(500, 500);

    var gui = new dat.GUI();
    gui.add(controller, "layers", 5, 15).step(1);
    gui.add(controller, "samples", 10, 30).step(1);

}

function draw() {

    background(255);

    var layers = controller.layers;
    var samples = controller.samples * layers;

    noFill();

    strokeWeight(2);
    stroke(color(40, 40, 255));

    beginShape();

    var centerX = width / 2;
    var centerY = height / 2;

    var radius = 0;
    var inc = 0.1;

    var offset = frameCount * 0.1; 

    for(var i = 0; i < samples; i++) {
        var p = i / samples;
        var r = p * layers;
        var theta = r * TWO_PI - offset;

        var x = cos(theta) * radius;
        var y = sin(theta) * radius;

        vertex(centerX + x, centerY + y);

        radius += inc;
        inc *= 1.02;
    }

    endShape();

}

