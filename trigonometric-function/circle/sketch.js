/*
 * sketch.js
 * */

var controller = {
    samples     : 10,
    radius      : 50,
};

function setup() {

    createCanvas(500, 500);

    var gui = new dat.GUI();
    gui.add(controller, "samples", 3, 30).step(1);
    gui.add(controller, "radius", 1, 50).step(1);

}

function draw() {

    background(255);

    var samples = controller.samples;
    var radius = controller.radius;

    noFill();

    strokeWeight(2);
    stroke(color(255, 40, 40));

    ellipse(200, 200, 50, 50);

    stroke(color(40, 40, 255));

    beginShape();

    var centerX = 400;
    var centerY = 200;

    for(var i = 0; i < samples; i++) {

        var r = i / samples; // 0.0 ~ 1.0まで変化する値
        var theta = r * TWO_PI; // 角度

        var x = cos(theta) * radius;
        var y = sin(theta) * radius;
        vertex(centerX + x, centerY + y);

    }

    endShape(CLOSE);

}

