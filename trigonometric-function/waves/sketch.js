/*
 * sketch.js
 * */

var controller = {
    samples     : 50,
    speed       : 0.025,
    fineness    : 5.0,
    intensity   : 0.2,
    offset      : 0.5
};

function setup() {

    createCanvas(500, 500);

    var gui = new dat.GUI();
    gui.add(controller, "samples", 2, 50, 1);
    gui.add(controller, "speed", 0, 1, 0.01);
    gui.add(controller, "fineness", 0, 20, 0.01);
    gui.add(controller, "intensity", 0, 1, 0.01);
    gui.add(controller, "offset", 0, 1, 0.01).listen();

}

function drawWave(t, samples, intensity, fineness, offset) {

    beginShape();

    var dy = height * offset;

    for(var i = 0; i < samples; i++) {
        var r = i / (samples - 1);

        var x = r * width;

        var theta = (r * fineness) + t;

        var y = (((sin(theta)) + 1.0) * 0.5) * (height * intensity);
        vertex(x, height - y + dy);

    }

    vertex(width, height);
    vertex(0, height);

    endShape(CLOSE);

}

function draw() {

    background(255);

    var samples = controller.samples;
    var speed = controller.speed;
    var intensity = controller.intensity;
    var fineness = controller.fineness;
    var offset = controller.offset;

    var t = frameCount * speed;

    // black large wave
    fill(color(0, 0, 0, 255));
    drawWave(t * 0.5, samples, intensity, fineness, offset);

    // gray medium wave
    fill(color(100, 100, 100, 255));
    drawWave(t * 0.8, samples, intensity * 0.6, fineness * 1.5, offset + cos(t) * 0.1);

    // white small wave
    fill(color(200, 200, 200, 255));
    drawWave(t * 1.1, samples, intensity * 0.3, fineness * 2.0, offset + 0.1 + cos(t) * 0.05);

    controller.offset = - 0.5 + sin(t) * 0.1;
  
}
