/*
 * sketch.js
 * */

var controller = {
    samples     : 100,
    speed       : 0.05,
    fineness    : 10.0,
    intensity   : 0.5 
};

function setup() {

    createCanvas(500, 500);

    var gui = new dat.GUI();
    gui.add(controller, "samples", 1, 200, 1);
    gui.add(controller, "speed", 0, 0.2, 0.01);
    gui.add(controller, "fineness", 0, 20, 0.01);
    gui.add(controller, "intensity", 0.1, 10, 0.01);

}

function draw() {

    background(255);

    var samples = controller.samples;
    var speed = controller.speed;
    var intensity = controller.intensity;
    var fineness = controller.fineness;

    var t = frameCount * speed;

    for(var i = 0; i < samples; i++) {
        var r = i / samples;

        var x = r * width;

        var theta = (r * fineness) + t;
        var y = ((sin(theta) + 1.0) * 0.5) * (height * intensity);
        // var y = (((sin(theta) + sin(theta * 1.2)) + 1.0) * 0.5) * (height * intensity);

        rect(x, y, 1, height - y);
    }
  
}
